
#include <iostream>

class Kawabanga
{
private:
    int f;
    
public:
    Kawabanga() : f(2)
    {}

    Kawabanga(int newA) : f(newA)
    {}

    int GetA()
    {
        return f;
    }

    void SetF(int newA)
    {
        f = newA;
    }
};

class Vector
{
public:
    
    Vector() : x(0), y(0), z(0)
    {}
    
    Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
    {}
    
    void Show()
    {
        std::cout << '\n' << x << ' ' << y << ' ' << z;
    }

    double Module()
    {
        return sqrt(pow(x, 2) + pow(y, 2) + pow(z, 2));
    }

private:
    double x;
    double y;
    double z;
};

int main()
{
    Kawabanga temp(11);
    std::cout << temp.GetA();

    Vector v;
    v.Show();
}

